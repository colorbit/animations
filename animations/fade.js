/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.colors = [];
    if (params.Random_Color) {
      state.colors[0] = rgb2hsv(colors.random());
      state.colors[1] = rgb2hsv(colors.random());
    } else {
      params.Color.forEach(function hsv(c) {
        state.colors.push(rgb2hsv(c));
      });
    }

    state.current = 0;
    state.target = 1;
    state.step = 0;
    state.donefading = 127;
  },

  anim() {
    var speed = lib8tion.cubicwave8(state.step);
    var current = state.colors[state.current];
    var target = state.colors[state.target];
    var color = blend_hsv(current, target, speed);
    var colorLength = params.Color.length;

    state.step += 1;

    if (state.step > state.donefading) {
      state.step = 0;

      if (params.Random_Color) {
        state.colors[0] = state.colors[1];
        state.colors[1] = rgb2hsv(colors.random());
      } else {
        state.current += 1;
        state.target += 1;

        if (state.current >= colorLength) {
          state.current = 0;
        }

        if (state.target >= colorLength) {
          state.target = 0;
        }
      }
    }

    LEDS[0] = hsv2rgb(color);
    mirror(LEDS, 1);
    show(LEDS);
    delay(params.Animation_Speed);
  },

  params: {
    Color: types.array(
      [
        colors.Red,
        colors.Blue,
        colors.Green,
      ],
      types.rgb(),
      descriptions.Colors
    ),
    Animation_Speed: types.number(20, descriptions.AnimationSpeed),
    Random_Color: types.bool(false, descriptions.RandomAnimationColors),
  },

  name: 'Fade',
  description: 'Fade between colors',
};
