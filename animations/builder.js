/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.width = LEDS.length / params.Number_Of_Builders;
    state.position = state.width;
    state.builderPos = 0;
    state.color = params.Color[0];
    state.currentColor = 0;
    state.lastColor = params.Color.length;
  },

  anim() {
    var i;
    var Builder_Width = params.Builder_Width;
    var builderPos = state.builderPos;
    var color = state.color;

    if (params.Every_LED_Random_Color) {
      state.color = colors.random();
    }

    if (builderPos < state.position) {
      // while the builder is moving
      for (i = 0; i < Builder_Width; i += 1) {
        if (builderPos - i >= 0) {
          LEDS[builderPos - i] = color;
        }
      }
      if (builderPos - Builder_Width >= 0) {
        LEDS[builderPos - Builder_Width] = colors.Black;
      }
      mirror(LEDS, state.width);
      show(LEDS);
      delay(params.Animation_Speed);
      state.builderPos += 1;
    } else {
      // build complete
      state.builderPos = 0;
      // light string is full
      if (state.position <= 1) {
        state.position = state.width + params.Builder_Width;
        fade_all_to_black(LEDS, 20);
        // change color, random first
        if (params.Every_LED_Random_Color === true
          || params.Random_Color === true) {
          state.color = colors.random();
        } else {
          // not random
          if (state.currentColor === state.lastColor - 1) {
            state.currentColor = 0;
          } else {
            state.currentColor += 1;
          }
          state.color = params.Color[state.currentColor];
        }
        // light string not full
      } else {
        state.position -= Builder_Width;
      }
    }
  },

  params: {
    Color: types.array(
      [
        colors.Red,
        colors.Blue,
        colors.Green,
      ],
      types.rgb(),
      descriptions.Colors
    ),
    Number_Of_Builders: types.number(2, descriptions.NumberOfAnimations),
    Builder_Width: types.number(5, 'Width of builder'),
    Animation_Speed: types.number(20, descriptions.AnimationSpeed),
    Random_Color: types.bool(false, descriptions.RandomAnimationColors),
    Every_LED_Random_Color: types.bool(false, descriptions.EveryLedRandomColor),
  },

  name: 'Builder',
  description: 'Builds the string with blocks of color',
};
