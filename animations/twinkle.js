/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.lastColor = params.Color.length;
  },

  anim() {
    var i;
    var colorIndex;
    var lastColor = state.lastColor;
    var Minimum_Lights = params.Minimum_Lights;
    var Maximum_Lights = params.Maximum_Lights;
    var Colors = params.Color;

    fade_all_to_black(LEDS, params.Fade_Speed);

    for (colorIndex = 0; colorIndex < lastColor; colorIndex += 1) {
      const num_color = random_int(Minimum_Lights, Maximum_Lights);
      for (i = 0; i < num_color; i += 1) {
        LEDS[random_int(0, NUM_LEDS)] = Colors[colorIndex];
      }
    }
    show(LEDS);
    delay(params.Animation_Speed);
  },

  params: {
    Color: types.array(
      [
        colors.Red,
        colors.Blue,
        colors.Green,
      ],
      types.rgb(),
      descriptions.Colors
    ),
    Animation_Speed: types.number(100, descriptions.AnimationSpeed),
    Fade_Speed: types.number(20, descriptions.FadeToBlackSpeed),
    Minimum_Lights: types.number(1, descriptions.MinimumLights),
    Maximum_Lights: types.number(3, descriptions.MaximumLights),
  },

  name: 'Twinkle',
  description: 'Twinkling lights of different colors.',
};
