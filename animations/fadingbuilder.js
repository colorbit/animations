/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.width = LEDS.length / params.Number_Of_Builders;
    state.position = state.width;
    state.builderPos = 0;
    // start fade variables
    state.colors = [];
    if (params.Random_Color) {
      state.colors[0] = rgb2hsv(colors.random());
      state.colors[1] = rgb2hsv(colors.random());
    } else {
      params.Color.forEach(function hsv(c) {
        state.colors.push(rgb2hsv(c));
      });
    }
    state.current = 0;
    state.target = 1;
    state.step = 0;
    state.donefading = 127;
    state.stepIncrease = params.Color_Blend_Speed;
  },

  anim() {
    var i;
    var j;
    var width = state.width;
    var donefading = state.donefading;
    var stepIncrease = state.stepIncrease;
    var Builder_Width = params.Builder_Width;
    var speed = lib8tion.cubicwave8(state.step);
    var current = state.colors[state.current];
    var target = state.colors[state.target];
    var color = hsv2rgb(blend_hsv(current, target, speed));
    var Random_Color = params.Random_Color;
    var colorLength = state.colors.length;

    if (state.builderPos < state.position) {
      // while the builder is moving
      for (i = 0; i < Builder_Width; i += 1) {
        if (state.builderPos - i >= 0) {
          LEDS[state.builderPos - i] = color;
        }
      }
      if (state.builderPos - Builder_Width >= 0) {
        LEDS[state.builderPos - Builder_Width] = colors.Black;
      }
      mirror(LEDS, width);
      show(LEDS);
      delay(params.Animation_Speed);
      state.builderPos += 1;
    } else {
      // build complete
      // blend the colors then continue
      // the first led is the correct color already
      for (j = 1; j < Builder_Width; j += 1) {
        state.step += stepIncrease;
        if (state.step > donefading) {
          state.step = 0;

          if (Random_Color) {
            state.colors[0] = state.colors[1];
            state.colors[1] = rgb2hsv(colors.random());
          } else {
            state.current += 1;
            state.target += 1;

            if (state.current >= colorLength) {
              state.current = 0;
            }

            if (state.target >= colorLength) {
              state.target = 0;
            }
          }
          current = state.colors[state.current];
          target = state.colors[state.target];
        }
        speed = lib8tion.cubicwave8(state.step);
        LEDS[state.builderPos - j - 1] = hsv2rgb(
          blend_hsv(current, target, speed)
        );
        mirror(LEDS, width);
        show(LEDS);
        delay(params.Animation_Speed);
      }
      state.builderPos = 0;
      // light string is full
      if (state.position <= 1) {
        state.position = state.width + params.Builder_Width;
        fade_all_to_black(LEDS, 20);
        // light string not full
      } else {
        state.position -= params.Builder_Width;
      }
    }
  },

  params: {
    Color: types.array(
      [
        colors.Red,
        colors.Blue,
        colors.Green,
      ],
      types.rgb(),
      descriptions.Colors
    ),
    Number_Of_Builders: types.number(1, descriptions.NumberOfAnimations),
    Builder_Width: types.number(10, 'Width of builder'),
    Animation_Speed: types.number(20, descriptions.AnimationSpeed),
    Color_Blend_Speed: types.number(3, descriptions.BlendSpeed),
    Random_Color: types.bool(false, descriptions.RandomAnimationColors),
  },

  name: 'Builder (Blending Colors)',
  description: 'Builds the string with blocks of color',
};
