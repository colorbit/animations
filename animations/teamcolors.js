/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {

  init() {
    var i;
    var j;
    var current;
    var target;
    var stepSpeed;
    var colorsLength = params.Color.length;
    var stepAmount = 127 / params.Width_Of_Blocks;

    state.waveWidth = params.Color.length * params.Width_Of_Blocks;
    state.init = false;
    state.moveStart = 0;
    state.moveCache = [];

    for (i = 0; i < colorsLength; i += 1) {
      if (params.Blend) {
        current = rgb2hsv(params.Color[i]);
        if (i === colorsLength - 1) {
          target = rgb2hsv(params.Color[0]);
        } else {
          target = rgb2hsv(params.Color[i + 1]);
        }
        for (j = 0; j < params.Width_Of_Blocks; j += 1) {
          stepSpeed = lib8tion.cubicwave8(stepAmount * j);
          state.moveCache.push(hsv2rgb(blend_hsv(current, target, stepSpeed)));
        }
      } else {
        for (j = 0; j < params.Width_Of_Blocks; j += 1) {
          state.moveCache.push(params.Color[i]);
        }
      }
    }
  },

  anim() {
    var i;
    var drawPos;
    var Animation_Speed = params.Animation_Speed;
    var Move = params.Move;
    var waveWidth = state.waveWidth;
    var moveCache = state.moveCache;
    var moveStart = state.moveStart;
    var init = state.init;

    if (Move) {
      for (i = 0; i < NUM_LEDS && i < waveWidth; i += 1) {
        drawPos = i + moveStart;
        LEDS[i] = moveCache[drawPos % waveWidth];
      }
      mirror(LEDS, waveWidth);
      show(LEDS);
      delay(Animation_Speed);
      state.moveStart += 1;
      if (moveStart >= waveWidth) {
        moveStart = 0;
      }
    } else if (!init) {
      for (i = 0; i < waveWidth && i < NUM_LEDS; i += 1) {
        LEDS[i] = moveCache[i];
      }
      mirror(LEDS, waveWidth);
      show(LEDS);
      state.init = true;
    }
  },

  params: {
    Color: types.array([{
      r: 127,
      g: 150,
      b: 149,
    }, {
      r: 0,
      g: 34,
      b: 68,
    }, {
      r: 134,
      g: 147,
      b: 151,
    }, {
      r: 0,
      g: 53,
      b: 148,
    }], types.rgb(), descriptions.Colors),
    Width_Of_Blocks: types.number(15, 'Width of each color block.'),
    Animation_Speed: types.number(100, descriptions.Animation_Speed),
    Move: types.bool(false,
      'Move the blocks of colors across the string of lights.'),
    Blend: types.bool(false, descriptions.BlendColors),
  },

  name: 'Color Blocks (Team Colors)',
  description: 'Still or moving blocks of color',
};
