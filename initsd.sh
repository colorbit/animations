#!/bin/bash

IP=$1
URL="http://${IP}"

for module in modules/*.js; do
    echo -n "Uploading module: ${module}... "
    name=$(basename "${module}")
    curl \
        -XPUT \
        --data-binary "@${module}" \
        "${URL}/module/${name}"
done

NUM_ANIMATIONS="25"
[[ "$(command -v jq)" ]] && \
    NUM_ANIMATIONS=$(curl -s "${URL}/anim" | jq '.animations | length')

echo "Deleting ${NUM_ANIMATIONS} animations"
for i in $(seq 0 "${NUM_ANIMATIONS}"); do
    file=$(printf "%03X" "${i}")
    curl -XDELETE "${URL}/anim/${file}"
done

for anim in animations/*.js; do
    echo -n "Uploading animation: ${anim}... "
    curl -XPOST --data-binary "@${anim}" "${URL}/anim"
done

echo -n "Setting autostart animation... "
curl -XPOST --data-binary "@animations/fadingblocksofcolor.js" "${URL}/autostart"
