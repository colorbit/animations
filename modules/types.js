/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
/* eslint-disable prefer-template */
module.exports = {
  number(default_value, description) {
    var def = default_value;
    var desc = description;

    if (default_value === undefined) {
      def = 0;
    }

    if (description === undefined) {
      desc = 'A number';
    }

    return {
      type: 'number',
      description: desc,
      parse(value) {
        if (value === undefined) {
          return def;
        }

        return JSON.parse(value);
      },
    };
  },

  bool(default_value, description) {
    var def = default_value;
    var desc = description;

    if (default_value === undefined) {
      def = false;
    }

    if (description === undefined) {
      desc = 'Yes/No';
    }

    return {
      type: 'bool',
      description: desc,
      parse(value) {
        if (value === undefined) {
          return def;
        }

        return JSON.parse(value);
      },
    };
  },

  rgb(default_color, description) {
    var def = default_color;
    var desc = description;

    if (default_color === undefined) {
      def = { r: 0, g: 0, b: 0 };
    }

    if (description === undefined) {
      desc = 'RGB Color';
    }

    return {
      type: 'rgb',
      description: desc,
      parse(value) {
        if (value === undefined) {
          return def;
        }

        return JSON.parse(value);
      },
    };
  },

  hsv(default_color, description) {
    var def = default_color;
    var desc = description;

    if (default_color === undefined) {
      def = { h: 0, s: 0, v: 0 };
    }

    if (description === undefined) {
      desc = 'HSV Color';
    }

    return {
      type: 'hsv',
      description: desc,
      hsv(value) {
        if (value === undefined) {
          return def;
        }

        return JSON.parse(value);
      },
    };
  },

  array(default_values, subtype, description) {
    var def = default_values;
    var st = subtype;
    var desc = description;

    if (default_values === undefined) {
      def = [];
    }

    if (subtype === undefined) {
      st = {
        type: 'unknown',
        description: 'unknown',
        parse(value) {
          return value;
        },
      };
    }

    if (description === undefined) {
      desc = 'Array of ' + st.description;
    }

    return {
      type: 'array',
      description: desc,
      subtype: st,
      parse(value) {
        var values;
        var i;
        var result = [];

        if (value === undefined) {
          values = def;
        } else {
          values = JSON.parse(value);
        }

        for (i = 0; i < values.length; i += 1) {
          result.push(subtype.parse(JSON.stringify(values[i])));
        }

        return result;
      },
    };
  },
};
