/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * init is special so we disable some eslint things */
/* eslint-disable prefer-destructuring, no-unused-vars */
module.exports = function init(env) {
  var scope = {
    Math,
    JSON,
    Object,
    Array,
    undefined,

    console: {
      log(x) {
        env.console.log(JSON.stringify(x));
      },
      error(x) {
        env.console.error(JSON.stringify(x));
      },
    },
    leds: env.leds,
    lib8tion: env.lib8tion,
    delay: env.delay,

    hsv2rgb: env.leds.hsv2rgb,
    rgb2hsv: env.leds.rgb2hsv,
    mirror: env.leds.mirror,
    reset: env.leds.reset,
    show: env.leds.show,
    scale8: env.lib8tion.scale8,
    beat8: env.lib8tion.beat8,
    beat16: env.lib8tion.beat16,
    beatsin8: env.lib8tion.beatsin8,
    beatsin16: env.lib8tion.beatsin16,
    random8: env.lib8tion.random8,
    random16: env.lib8tion.random16,
    random_int: env.lib8tion.random16,

    fade_to_black: env.leds.fade_to_black,
    fade_to_color: env.leds.fade_to_color,
    fill_gradient: env.leds.fill_gradient,
    blend_hsv: env.leds.blend_hsv,

    LEDS: env.leds.leds,
    NUM_LEDS: env.leds.leds.length,

    stop: env.animation.stop,

    palettes: env.palettes,
    RGBPalette: env.palettes.RGBPalette,
    HSVPalette: env.palettes.HSVPalette,
    RGBGradientPalette: env.palettes.RGBGradientPalette,

    fade_all_to_black(strand, factor) {
      env.leds.fade_to_black(strand, 0, env.leds.leds.length - 1, factor);
    },

    fade_all_to_color(strand, color, factor) {
      env.leds.fade_to_color(
        strand,
        color,
        0,
        env.leds.leds.length - 1,
        factor
      );
    },

    golden_random(saturation, value) {
      var h = Math.random();
      var s = saturation;
      var v = value;

      if (s === undefined) {
        s = env.leds.random16(0, 255);
      }

      if (v === undefined) {
        v = env.leds.random16(0, 255);
      }

      h += 0.618033988749895;
      h %= 1;

      return env.leds.hsv2rgb({ h, s, v });
    },

    state: {},
    params: {},
    raw_params: env.params,
  };

  env.leds.reset(env.leds.leds);

  Object.keys(env.animation.animation.params).forEach(function p(k) {
    var animation = env.animation;
    var parse = env.animation.animation.params[k].parse;
    scope.params[k] = parse(animation.params[k]);
  });

  env.animation.run(env.animation.file, scope);
  env.leds.reset(env.leds.leds);
};
