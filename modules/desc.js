/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
module.exports = {
  Colors: 'A list of each color to be used in the animation.',
  AnimationSpeed: [
    'Amount of time between one animation frame and the next.',
    '(min = 1 fast, max = 5000 slow)',
  ].join(' '),
  FadeToBlackSpeed: [
    'How fast a lit LED fades to off by number of animation frames.',
    '(min = 0 off, max = 1000 fast)',
  ].join(' '),
  WrapAround: [
    'Animation appears to move off the end of the string of LEDs and back to',
    'the beginning.',
  ].join(' '),
  RandomAnimationColors: [
    'Each time the animation completes, a new random color will be used for',
    'the animation, color selections are ignored.',
  ].join(' '),
  EveryLedRandomColor: [
    'Each LED is assigned a completely random color, color selections are ',
    'ignored.',
  ].join(' '),
  NumberOfAnimations: [
    'Number of animations displayed on the string.',
  ].join(' '),
  MinimumLights: [
    'Minimum number of lights added for each color in each animation frame.',
  ].join(' '),
  MaximumLights: [
    'Maximum number of lights added for each color in each animation frame.',
  ].join(' '),
  BlendSpeed: [
    'The speed (abruptness) that one color blends to the next.',
    '(1 smooth, 10 abrupt)',
  ].join(' '),
  BlendColors: 'Blend one color into the next.',
};
